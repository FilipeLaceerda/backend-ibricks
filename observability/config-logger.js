const { createLogger, transports, format, Logger } = require("winston");
const LokiTransport = require("winston-loki");

let logger;

const initializeLogger = () => {
    if (logger) {
        return
    }

    logger = createLogger({
        transports: [new LokiTransport({
            host: "http://127.0.0.1:3001/loki/api/v1/push",
            labels: { app: 'ibricks'},
            json: true,
            format: format.json(),
            replaceTimestamp: true,
            onConnectionError: (err) => console.error(err)
        }),
            new transports.Console({
                format: format.combine(format.simple(), format.colorize())
            })]
    })
}

const getLogger = () => {
    initializeLogger();
    return logger;
};

module.exports = {
    getLogger,
};