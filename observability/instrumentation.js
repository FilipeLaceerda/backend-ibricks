const opentelemetry = require('@opentelemetry/sdk-node');
const {
  getNodeAutoInstrumentations,
} = require('@opentelemetry/auto-instrumentations-node');
const {
  OTLPTraceExporter,
} = require('@opentelemetry/exporter-trace-otlp-proto');
const { OTLPLogExporter } = require('@opentelemetry/exporter-logs-otlp-proto');
const { PrometheusExporter } = require('@opentelemetry/exporter-prometheus');
const { diag, DiagConsoleLogger, DiagLogLevel } = require('@opentelemetry/api');

// Configuração do logger
//diag.setLogger(new DiagConsoleLogger(), DiagLogLevel.ALL);

const traceExporter = new OTLPTraceExporter({
  url: 'http://localhost:4318/v1/traces',
});

const logExporter = new OTLPLogExporter({
  url: 'http://localhost:3500/loki/api/v1/push',
});

const sdk = new opentelemetry.NodeSDK({
  traceExporter: traceExporter,
  logExporter: logExporter,
  metricReader: new PrometheusExporter({
    host: 'localhost',
    port: 4317,
  }),
  resource: new Resource({
    [SemanticResourceAttributes.SERVICE_NAME]: 'service',
  }),
  instrumentations: [getNodeAutoInstrumentations()],
});
sdk.start();
