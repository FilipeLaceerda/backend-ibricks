const express = require('express');
const app = express();
const port = 3003;
app.use(express.json());
const feedsRoutes = require('../routes/store_route');

app.use('', feedsRoutes);


app.listen(port, () => {
    console.log(`Servidor de lojas rodando em http://localhost:${port}`);
});