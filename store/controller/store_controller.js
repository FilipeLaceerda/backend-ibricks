const {PrismaClient} = require('@prisma/client');
const prisma = new PrismaClient();
const { trace, context, metrics } = require('@opentelemetry/api');
const winston = require('winston')
const LokiTransport = require('winston-loki');
const { NodeTracerProvider } = require('@opentelemetry/sdk-trace-node');
const { SimpleSpanProcessor } = require('@opentelemetry/sdk-trace-base');
require('../metrics');
const { OTLPTraceExporter } = require('@opentelemetry/exporter-trace-otlp-grpc');
const {Resource} = require("@opentelemetry/resources");
const {SEMRESATTRS_SERVICE_NAME} = require("@opentelemetry/semantic-conventions");

const provider = new NodeTracerProvider({
    resource: new Resource({
        [SEMRESATTRS_SERVICE_NAME]: 'STORE',
    }),
});
const exporter = new OTLPTraceExporter({
    url: 'http://otel-collector:4317/v1/traces',
});
provider.addSpanProcessor(new SimpleSpanProcessor(exporter));
provider.register();
const logger = winston.createLogger({
    level: 'debug',
    format: winston.format.json(),
    transports: [
        new winston.transports.Console(),
        new LokiTransport({
            host: 'http://loki:3100',
            json: true,
            labels: { job: 'store' }
        })
    ]
});
const meter = metrics.getMeter("express-server");
let counter = meter.createCounter("store-service", {
  description: "The number of requests per name the server got",
});
let histogram = meter.createHistogram("histogram.store")
const getStores = async (req, res, time) => {

    const tracer = trace.getTracer('default');
    const span = tracer.startSpan('store', {
        attributes: {
            method: req.method,
            url: req.url
        }
    });
    counter.add(1, {method: req.method, status: req.status})
    const startTime = Date.now();
    try {
        span.addEvent('DB connection start');
        await prisma.$connect();
        span.addEvent('DB connection established');
        const stores = await prisma.lojas.findMany();
        if (!stores) {
            span.addEvent('Sem lojas cadastradas');
            logger.info({ message: `method=${req.method} url=${req.url} status=${req.status} `, labels: { 'service': 'store' } })
            res.status(204).json({ mensagem: 'Não há lojas cadastradas.' });
            return;
        }
        const endTime = Date.now();
        const executionTime = endTime - startTime;
        histogram.record(executionTime);
        logger.info({ message: `method=${req.method} url=${req.url} status=${res.statusCode} `, labels: { 'service': 'store' } })
        res.json(stores);
    } catch (e) {
        span.setStatus({ code: 'ERROR', message: e.message });
        span.addEvent('Error fetching stores', { error: e.message });
        logger.error({ message: `method=${req.method} url=${req.url} status=${res.statusCode} error=${e.stack}`, labels: { 'service': 'store' } })
        throw e;
    } finally {
        await prisma.$disconnect();
        console.log(span);
        span.end();
    }
};

const getListMachines = async (req, res) => {
    //const span = tracer.startSpan('list-machines');
    try {
        let storesId = parseInt(req.params.storesId);
        const machines = await prisma.ferramentas.findMany({
            where: { stores_id: storesId }
        });
        res.json(machines);
    } catch (e) {
        //span.setStatus({ code: 'ERROR', message: e.message });
        throw e;
    } finally {
        prisma.$disconnect();
        //span.end();
    }
}

const getAllMachinesForFeed = async (req, res) => {
    try {
        const machineFeeds = await prisma.ferramentas.findMany();
        res.json(machineFeeds);
    } catch (e) {
        span.setStatus({ code: 'ERROR', message: e.message });
        res.status(500).json({ mensagem: 'Erro ao buscar máquinas.', error: e.error });
    } finally {
        prisma.$disconnect()
    }
}

const createStore = async (req, res) => {
    const span = tracer.startSpan('create-store');
    try {
        const store = await prisma.lojas.create({
            data: {}
        });
        res.json(store);
    } catch (e) {
        span.setStatus({ code: 'ERROR', message: e.message });
        res.status(500).json({ mensagem: 'Erro ao criar lojas.', error: e.error });
    } finally {
         span.end();
        await prisma.$disconnect();
    }
}

const createMachine = async (req, res) => {
    const span = tracer.startSpan('create-machine');
    try {
        const machine = await prisma.ferramentas.create({
            data: {}
        });
        res.json(machine);
    } catch (e) {
        span.setStatus({ code: 'ERROR', message: e.message });
        res.status(500).json({ mensagem: 'Erro ao criar máquinas.', error: e.error });
    } finally {
         span.end();
        await prisma.$disconnect();
    }
}

const helloWorld = async (req, res) => {
    res.status(200).json({message: 'Hello world'});
}

module.exports = { getStores, getListMachines, getAllMachinesForFeed, createStore, createMachine, helloWorld };


