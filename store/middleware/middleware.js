const authenticateToken = (req, res, next) => {
    const token = req.headers.authorization;

    if (!token) {
        return res.status(401).json({ error: 'Token não fornecido' });
    }

    if (token === 'seu_token') {
        next();
    } else {
        res.status(403).json({ error: 'Acesso não autorizado' });
    }
};

module.exports = { authenticateToken }