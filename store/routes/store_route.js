const express = require('express');
const router = express.Router();
const storesController = require('../controller/store_controller');
const Prometheus = require('prom-client');
const register = new Prometheus.Registry();

const httpRequestTimer = new Prometheus.Histogram({
    name: 'http_request_duration_seconds',
    help: 'Duration of HTTP requests in seconds',
    labelNames: ['method', 'route', 'code'],
    buckets: [0.1, 0.3, 0.5, 0.7, 1, 3, 5, 7, 10]
});

register.registerMetric(httpRequestTimer);

router.get('/stores', async (req, res) => {
    const end = httpRequestTimer.startTimer();
    const route = req.route.path;
    await storesController.getStores(req, res);
    end({ route, code: res.statusCode, method: req.method });
});
router.get('/machines', storesController.getAllMachinesForFeed);
router.post('/machine/:storeId', storesController.createMachine);
router.get('/stores/:storesId/machines', storesController.getListMachines);
router.post('/store/:usuarioId', storesController.createStore);
router.get('', storesController.helloWorld);
router.get('/metrics', async (req, res) => {
    const end = httpRequestTimer.startTimer();
    const route = req.route.path;
    res.setHeader('Content-Type', register.contentType);
    res.send(await register.metrics());
    end({ route, code: res.statusCode, method: req.method });
});


module.exports = router;