const jwt = require('jsonwebtoken');

const segredoJWT = 'seuSegredoJWT'; // Substitua com um segredo mais seguro na produção

function gerarToken(usuario) {
    return jwt.sign({ id: usuario.id }, segredoJWT, { expiresIn: '1h' });
}

function verificarToken(token) {
    try {
        const decoded = jwt.verify(token, segredoJWT);
        return decoded;
    } catch (err) {
        return null;
    }
}

module.exports = {
    gerarToken,
    verificarToken,
};
