const express = require('express');
const router = express.Router();
const usuariosController = require('../controllers/users.controllers');
const auth = require('../auth');

router.get('/usuarios', usuariosController.listarUsuarios);
router.post('/sign-up', usuariosController.criarUsuario);


router.post('/login', async (req, res) => {
    const { email, senha } = req.body;
    try {
        const usuario = await usuariosController.encontrarUsuarioPorEmailESenha(email, senha);
        if (usuario) {
            res.json({usuario: usuario});
        } else {
            res.status(401).json({ mensagem: 'Credenciais inválidas' });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({ mensagem: 'Ocorreu um erro ao processar a solicitação', error: error });
    }
});

router.get('/', (req, res) => res.send('Hello API'));

module.exports = router;
