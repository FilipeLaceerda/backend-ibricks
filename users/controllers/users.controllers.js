const {PrismaClient} = require('@prisma/client');
const prisma = new PrismaClient();

const bcrypt = require('bcrypt');
const {trace} = require('@opentelemetry/api');

const listarUsuarios = async (req, res) => {
    const usuarios = await prisma.usuario.findMany();
    res.json(usuarios);
};

const criarUsuario = async (req, res) => {
    const saltRounds = 15;
    const {email, senha, nome, endereco, numeroTelefone, tipo, documento, descricao} = req.body;
    const hashedSenha = await bcrypt.hash(senha, saltRounds);

    try {
        const novoUsuario = await prisma.usuario.create({
            data: {
                email,
                senha: hashedSenha,
                nome,
                endereco,
                numeroTelefone,
                tipo,
            },
        });

        if (novoUsuario.tipo === 'INDIVIDUAL') {
            const pessoaFisica = await prisma.usuarioIndividual.create({
                data: {
                    usuarioId: novoUsuario.id,
                    cpf: documento,
                }
            });

            const permissaoPessoaFisica = await prisma.permissao.create({
                data: {
                    usuarioId: novoUsuario.id,
                    tipo: 'INDIVIDUAL',
                }
            });

            let retorno = Object.assign({}, novoUsuario, pessoaFisica, permissaoPessoaFisica);
            res.json(retorno);
        } else if (novoUsuario.tipo === 'EMPRESARIAL') {
            const pessoaJuridica = await prisma.usuarioEmpresarial.create({
                data: {
                    usuarioId: novoUsuario.id,
                    cnpj: documento,
                    descricao,
                }
            });

            // Crie a permissão para o usuário empresarial
            const permissaoPessoaJuridica = await prisma.permissao.create({
                data: {
                    usuarioId: novoUsuario.id,
                    tipo: 'EMPRESARIAL', // ou qualquer valor que represente a permissão para pessoa jurídica
                }
            });

            // Retorne todas as informações do novo usuário (incluindo pessoa jurídica e permissão)
            let retorno = Object.assign({}, novoUsuario, pessoaJuridica, permissaoPessoaJuridica);
            res.json(retorno);
        }
    } catch (error) {
        res.status(500).json({mensagem: 'Erro ao cadastrar usuário', erro: error.message});
    }
};

const encontrarUsuarioPorEmailESenha = async (email, senha) => {
    try {
        const usuario = await prisma.usuario.findUnique({
            where: {
                email,
            },
        });

        if (usuario) {
            return usuario;
        }

        return null;
    } catch (e) {
        console.log(e);
        return null;
    }
};

const atualizarUsuario = async (req, res) => {
    const {id, nome, endereco, numeroTelefone, tipo, documento, descricao} = req.body;

    try {
        const usuarioAtualizado = await prisma.usuario.update({
            where: {
                id,
            },
            data: {
                nome,
                endereco,
                numeroTelefone,
                tipo,
            },
        });

        if (usuarioAtualizado.tipo === 'INDIVIDUAL') {
            await prisma.usuarioIndividual.update({
                where: {
                    usuarioId: id,
                },
                data: {
                    cpf: documento,
                },
            });
        } else if (usuarioAtualizado.tipo === 'EMPRESARIAL') {
            await prisma.usuarioEmpresarial.update({
                where: {
                    usuarioId: id,
                },
                data: {
                    cnpj: documento,
                    descricao,
                },
            });
        }

        res.json(usuarioAtualizado);
    } catch (error) {
        res.status(500).json({mensagem: 'Erro ao atualizar usuário', erro: error.message});
    }
};

const deletarUsuario = async (req, res) => {
    const {id} = req.params;

    try {
        const usuarioDeletado = await prisma.usuario.delete({
            where: {
                id,
            },
        });

        res.json(usuarioDeletado);
    } catch (error) {
        res.status(500).json({mensagem: 'Erro ao deletar usuário', erro: error.message});
    }
};

module.exports = {listarUsuarios, criarUsuario, encontrarUsuarioPorEmailESenha}