const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());

const usuariosRoutes = require('../routes/user');
app.use('/api', usuariosRoutes);

app.listen(port, () => {
    console.log(`Servidor de usuários rodando em http://localhost:${port}`);
});
