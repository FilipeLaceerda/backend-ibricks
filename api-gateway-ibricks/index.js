const httpProxy = require('express-http-proxy');
const express = require('express');
const app = express();
const port = 8080;
const {
 USERS_API_URL, AVALIATION_API_URL, RESERVATION_API_URL, STORE_API_URL
} = require('./URLs');

const userServiceProxy = httpProxy(USERS_API_URL);
const storeServiceProxy = httpProxy(STORE_API_URL);
const avaliationServiceProxy = httpProxy(AVALIATION_API_URL);
const reservationServiceProxy = httpProxy(RESERVATION_API_URL);

app.get('/', (req, res) => res.send('Hello Gateway API'));

app.use('/login', (req, res, next) => {
 console.log(req.body)
 userServiceProxy(req, res, next)
});
app.get('/stores', (req, res, next) => storeServiceProxy(req, res, next));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));