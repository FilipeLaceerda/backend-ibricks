-- CreateTable
CREATE TABLE "store_review" (
    "id" SERIAL NOT NULL,
    "storeId" INTEGER NOT NULL,

    CONSTRAINT "store_review_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "rating" (
    "id" SERIAL NOT NULL,
    "rating" DOUBLE PRECISION NOT NULL,
    "storeReviewId" INTEGER NOT NULL,

    CONSTRAINT "rating_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "coments" (
    "id" SERIAL NOT NULL,
    "coment" TEXT NOT NULL,
    "storeReviewId" INTEGER NOT NULL,

    CONSTRAINT "coments_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "rating" ADD CONSTRAINT "rating_storeReviewId_fkey" FOREIGN KEY ("storeReviewId") REFERENCES "store_review"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "coments" ADD CONSTRAINT "coments_storeReviewId_fkey" FOREIGN KEY ("storeReviewId") REFERENCES "store_review"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
