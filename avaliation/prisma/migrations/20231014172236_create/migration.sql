/*
  Warnings:

  - You are about to drop the column `storeReviewId` on the `coments` table. All the data in the column will be lost.
  - You are about to drop the column `storeReviewId` on the `rating` table. All the data in the column will be lost.
  - You are about to drop the column `storeId` on the `store_review` table. All the data in the column will be lost.
  - Added the required column `store_review_id` to the `coments` table without a default value. This is not possible if the table is not empty.
  - Added the required column `store_review_id` to the `rating` table without a default value. This is not possible if the table is not empty.
  - Added the required column `store_id` to the `store_review` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "coments" DROP CONSTRAINT "coments_storeReviewId_fkey";

-- DropForeignKey
ALTER TABLE "rating" DROP CONSTRAINT "rating_storeReviewId_fkey";

-- AlterTable
ALTER TABLE "coments" DROP COLUMN "storeReviewId",
ADD COLUMN     "store_review_id" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "rating" DROP COLUMN "storeReviewId",
ADD COLUMN     "store_review_id" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "store_review" DROP COLUMN "storeId",
ADD COLUMN     "store_id" INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE "rating" ADD CONSTRAINT "rating_store_review_id_fkey" FOREIGN KEY ("store_review_id") REFERENCES "store_review"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "coments" ADD CONSTRAINT "coments_store_review_id_fkey" FOREIGN KEY ("store_review_id") REFERENCES "store_review"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
