
const {PrismaClient} = require('@prisma/client');
const prisma = new PrismaClient();
const { trace } = require('@opentelemetry/api');

const tracer = trace.getTracer(
    'avaliation-service',
    '0.1.0'
)


const getRating = async (req, res) => {
    const span = tracer.startSpan('rating')
    try{
    let storeReviewId = parseInt(req.params.storeReviewId);
    const rating = await prisma.rating.aggregate({
        _avg: {rating: true},
        where: {store_review_id: storeReviewId}
    });
    span.end();
    res.json(rating._avg);
    } catch (e) {
        span.end();
        res.json(e.message);
    } finally {
        span.end();
        prisma.$disconnect();
    }

}
const getComents = async (req, res) => {
    let storeReviewId = parseInt(req.params.storeReviewId);
    const coments = await prisma.coments.findMany({
        where: {store_review_id: storeReviewId}
    })
    res.json(coments);
}

const countComents = async (req, res) => {
    let storeReviewId = parseInt(req.params.storeReviewId);
    const countComents = await prisma.coments.count({
        where: {store_review_id: storeReviewId}
    });
    let countComent = {
        count: countComents
    }
    res.json(countComent);
}

const registerComents = async (req, res, next) => {
    const span = tracer.startSpan('register-coments');
    try {
        let storeReviewId = parseInt(req.params.storeReviewId);
        const registerComents = await prisma.coments.create({
            data: {
                coment: req.body.coment,
                store_review_id: storeReviewId
            }
        });
        span.end();
        return res.json(registerComents);
    } catch (e) {
        span.end();
        res.json({messagem: "Não foi possível criar uma avaliação", error: e.error});
    } finally {
        span.end();
        prisma.$disconnect();
    }
}

module.exports = { getRating, getComents, countComents, registerComents }