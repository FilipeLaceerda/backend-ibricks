const express = require('express');
const app = express();
const port = 3002;

app.use(express.json());

const feedsRoutes = require('../routes/avaliation_routes');
app.use('/api', feedsRoutes);

app.listen(port, () => {
    console.log(`Servidor de avaliações rodando em http://localhost:${port}`);
});