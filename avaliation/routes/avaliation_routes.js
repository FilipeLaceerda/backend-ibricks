const express = require('express');
const router = express.Router();
const avaliationController = require('../controller/avaliation_controller');

router.get('/avaliations/:storeReviewId/rating', avaliationController.getRating);
router.get('/avaliations/:storeReviewId/coments', avaliationController.getComents);
router.get('/avaliations/:storeReviewId/count-coments', avaliationController.countComents);
router.post('/avaliations/:storeReviewId/register-coments', avaliationController.registerComents);

module.exports = router;