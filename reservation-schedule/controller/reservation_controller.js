const {PrismaClient} = require('@prisma/client');
const prisma = new PrismaClient();

async function reservaDaFerramenta(req, res, next) {
    const {user_id, machine_id, startTime, endTime} = req.body;
    try {
        const reserva = await prisma.reserva.create({
            data: {
                user_id,
                machine_id,
                startTime,
                endTime
            }
        });
        return res.json(reserva);
    } catch (e) {
        res.status(500).json({mensagem: "Erro ao criar uma reserva", error: e.error})
    }
}

async function agendamentoDoAluguel(req, res, next) {
    const {user_id, machine_id, startDate, endDate} = req.body;
    try {

        const agendamento = await prisma.agendamento.create({
            data: {
                user_id,
                machine_id,
                startDate,
                endDate
            }
        });
        return res.json(agendamento);
    } catch (error) {
        res.status(500).json({mensagem: "Error em agendar!", error: error.error});
    }
}

module.exports = { reservaDaFerramenta, agendamentoDoAluguel }
