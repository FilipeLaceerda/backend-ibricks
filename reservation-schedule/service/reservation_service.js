const express = require('express');
const app = express();
const port = 3005;

app.use(express.json());

const reservationRoutes = require('../routes/reservation_routes');
app.use('/api', reservationRoutes);

app.listen(port, () => {
    console.log(`Servidor de usuários rodando em http://localhost:${port}`);
});
