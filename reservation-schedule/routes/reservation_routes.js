const express = require('express');
const router = express.Router();
const reservationController = require('../controller/reservation_controller');
const auth = require('../auth');

router.post('/reservation', reservationController.reservaDaFerramenta);
router.post('/schedule', reservationController.agendamentoDoAluguel);

module.exports = router;
