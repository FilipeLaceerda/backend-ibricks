#!/bin/sh

if [ -z "$1" ]; then
  echo "Uso: $0 <URL> [opções do Siege]"
  exit 1
fi

siege "$@"
