-- detail definition
-- Drop table
-- DROP TABLE IF EXISTS detail;

CREATE TABLE IF NOT EXISTS detalhe (
                                      id serial4 NOT NULL,
                                      detail_id int4 NOT NULL,
                                      descricao varchar(255) NOT NULL,
                                      tel varchar(255) NOT NULL,
                                      endereco varchar(255) NOT NULL,
                                      CONSTRAINT detail_detail_id_key UNIQUE (detail_id),
                                      CONSTRAINT detail_pkey PRIMARY KEY (id)
);

-- stores definition
-- Drop table
-- DROP TABLE IF EXISTS stores;

CREATE TABLE IF NOT EXISTS lojas (
                                      id serial4 NOT NULL,
                                      name varchar(255) NOT NULL,
                                      time varchar(255) NOT NULL,
                                      avaliation varchar(255) NOT NULL,
                                      price varchar(255) NOT NULL,
                                      image bytea NULL,
                                      detail_id int4 NULL,
                                      CONSTRAINT stores_pkey PRIMARY KEY (id),
                                      CONSTRAINT stores_detail_id_fkey FOREIGN KEY (detail_id) REFERENCES detalhe(id)
);

-- tool definition
-- Drop table
-- DROP TABLE IF EXISTS tool;

CREATE TABLE IF NOT EXISTS ferramentas (
                                    id serial4 NOT NULL,
                                    nome varchar(255) NOT NULL,
                                    tipo varchar(255) NOT NULL,
                                    disponivel bool NOT NULL,
                                    price_product float8 DEFAULT 0.0 NOT NULL,
                                    stores_id int4 NULL,
                                    CONSTRAINT tool_pkey PRIMARY KEY (id),
                                    CONSTRAINT tool_stores_id_fkey FOREIGN KEY (stores_id) REFERENCES lojas(id)
);

-- stock definition
-- Drop table
-- DROP TABLE IF EXISTS stock;

CREATE TABLE IF NOT EXISTS estoque (
                                     id serial4 NOT NULL,
                                     quantidade int4 NOT NULL,
                                     maquina_id int4 NULL,
                                     CONSTRAINT stock_pkey PRIMARY KEY (id)
);



-- stock foreign keys
ALTER TABLE estoque ADD CONSTRAINT stock_maquina_id_fkey FOREIGN KEY (maquina_id) REFERENCES ferramentas(id);


-- Inserir dados na tabela detalhe
INSERT INTO detalhe (detail_id, descricao, tel, endereco) VALUES
                                                              (1, 'Detalhe 1', '123456789', 'Endereço 1'),
                                                              (2, 'Detalhe 2', '987654321', 'Endereço 2');

-- Inserir dados na tabela lojas
INSERT INTO lojas (name, time, avaliation, price, detail_id) VALUES
                                                                 ('Loja 1', '08:00 - 18:00', '4.5', 'R$100', 1),
                                                                 ('Loja 2', '09:00 - 19:00', '4.8', 'R$150', 2);

-- Inserir dados na tabela ferramentas
INSERT INTO ferramentas (nome, tipo, disponivel, price_product, stores_id) VALUES
                                                                               ('Martelo', 'Ferramenta Manual', TRUE, 29.99, 1),
                                                                               ('Chave de Fenda', 'Ferramenta Manual', TRUE, 19.99, 1),
                                                                               ('Serra Elétrica', 'Ferramenta Elétrica', FALSE, 299.99, 2);

-- Inserir dados na tabela estoque
INSERT INTO estoque (quantidade, maquina_id) VALUES
                                                 (50, 1),
                                                 (30, 2),
                                                 (10, 3);


