-- public.usuario definition

-- Drop table

-- DROP TABLE public.usuario;

CREATE TYPE tipo_usuario AS ENUM ('empresarial', 'individual');


CREATE TABLE public.usuario (
                                id serial4 NOT NULL,
                                nome text NOT NULL,
                                endereco text NOT NULL,
                                "numeroTelefone" text NOT NULL,
                                email text NOT NULL,
                                tipo tipo_usuario NOT NULL,
                                senha text NOT NULL,
                                CONSTRAINT usuario_pkey PRIMARY KEY (id)
);
CREATE UNIQUE INDEX usuario_email_key ON public.usuario USING btree (email);


-- public."UsuarioEmpresarial" definition

-- Drop table

-- DROP TABLE public."UsuarioEmpresarial";

CREATE TABLE public."UsuarioEmpresarial" (
                                             id serial4 NOT NULL,
                                             "usuarioId" int4 NOT NULL,
                                             cnpj text NOT NULL,
                                             descricao text NOT NULL,
                                             CONSTRAINT "UsuarioEmpresarial_pkey" PRIMARY KEY (id)
);
CREATE UNIQUE INDEX "UsuarioEmpresarial_cnpj_key" ON public."UsuarioEmpresarial" USING btree (cnpj);
CREATE UNIQUE INDEX "UsuarioEmpresarial_usuarioId_key" ON public."UsuarioEmpresarial" USING btree ("usuarioId");


-- public."UsuarioEmpresarial" foreign keys

ALTER TABLE public."UsuarioEmpresarial" ADD CONSTRAINT "UsuarioEmpresarial_usuarioId_fkey" FOREIGN KEY ("usuarioId") REFERENCES public.usuario(id) ON DELETE RESTRICT ON UPDATE CASCADE;

-- public."usuarioIndividual" definition

-- Drop table

-- DROP TABLE public."usuarioIndividual";

CREATE TABLE public."usuarioIndividual" (
                                            id serial4 NOT NULL,
                                            "usuarioId" int4 NOT NULL,
                                            cpf text NOT NULL,
                                            CONSTRAINT "usuarioIndividual_pkey" PRIMARY KEY (id)
);
CREATE UNIQUE INDEX "usuarioIndividual_cpf_key" ON public."usuarioIndividual" USING btree (cpf);
CREATE UNIQUE INDEX "usuarioIndividual_usuarioId_key" ON public."usuarioIndividual" USING btree ("usuarioId");


-- public."usuarioIndividual" foreign keys


ALTER TABLE public."usuarioIndividual" ADD CONSTRAINT "usuarioIndividual_usuarioId_fkey" FOREIGN KEY ("usuarioId") REFERENCES public.usuario(id) ON DELETE RESTRICT ON UPDATE CASCADE;

INSERT INTO public.usuario (nome, endereco, "numeroTelefone", email, tipo, senha) VALUES
('Empresa A', 'Endereço A', '123456789', 'contato@empresaa.com', 'empresarial', 'senhaA'),
('Empresa B', 'Endereço B', '987654321', 'contato@empresab.com', 'empresarial', 'senhaB'),
('Individual A', 'Endereço C', '555555555', 'individuoA@email.com', 'individual', 'senhaC'),
('Individual B', 'Endereço D', '444444444', 'individuoB@email.com', 'individual', 'senhaD');

INSERT INTO public."UsuarioEmpresarial" ("usuarioId", cnpj, descricao) VALUES
(1, '12.345.678/0001-99', 'Descrição da Empresa A'),
(2, '98.765.432/0001-88', 'Descrição da Empresa B');

INSERT INTO public."usuarioIndividual" ("usuarioId", cpf) VALUES
(3, '123.456.789-00'),
(4, '987.654.321-00');
