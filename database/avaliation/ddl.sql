-- Criação da tabela 'store_review'
CREATE TABLE store_review (
                              id SERIAL PRIMARY KEY,
                              store_id INT NOT NULL
);

-- Criação da tabela 'rating'
CREATE TABLE rating (
                        id SERIAL PRIMARY KEY,
                        rating FLOAT NOT NULL,
                        store_review_id INT NOT NULL,
                        CONSTRAINT fk_store_review_rating
                            FOREIGN KEY (store_review_id)
                                REFERENCES store_review (id)
                                ON DELETE CASCADE
);

-- Criação da tabela 'coments'
CREATE TABLE coments (
                         id SERIAL PRIMARY KEY,
                         coment TEXT NOT NULL,
                         store_review_id INT NOT NULL,
                         CONSTRAINT fk_store_review_coments
                             FOREIGN KEY (store_review_id)
                                 REFERENCES store_review (id)
                                 ON DELETE CASCADE
);


INSERT INTO store_review (store_id) VALUES (1);
INSERT INTO store_review (store_id) VALUES (2);
INSERT INTO store_review (store_id) VALUES (3);

INSERT INTO rating (rating, store_review_id) VALUES (4.5, 1);
INSERT INTO rating (rating, store_review_id) VALUES (3.7, 2);
INSERT INTO rating (rating, store_review_id) VALUES (5.0, 3);
INSERT INTO rating (rating, store_review_id) VALUES (2.8, 1);

INSERT INTO coments (coment, store_review_id) VALUES ('Ótimo serviço!', 1);
INSERT INTO coments (coment, store_review_id) VALUES ('Preço justo.', 2);
INSERT INTO coments (coment, store_review_id) VALUES ('Atendimento excelente.', 3);
INSERT INTO coments (coment, store_review_id) VALUES ('Pouca variedade de produtos.', 1);
