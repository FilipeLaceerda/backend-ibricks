-- Create the Machine table
CREATE TABLE machine (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  location VARCHAR(255) NOT NULL
);

-- Create the MachineReservation table
CREATE TABLE machine_reservation (
  id SERIAL PRIMARY KEY,
  machine_id INT NOT NULL,
  start_time TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  end_time TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  status VARCHAR(255) NOT NULL,
  FOREIGN KEY (machine_id) REFERENCES Machine(id)
);

-- Create the User table
CREATE TABLE user (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL
);

-- Create the Schedule table
CREATE TABLE schedule (
  id SERIAL PRIMARY KEY,
  user_id INT NOT NULL,
  reservation_id INT NOT NULL,
  start_time TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  end_time TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  status VARCHAR(255) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES User(id),
  FOREIGN KEY (reservation_id) REFERENCES MachineReservation(id)
);

-- Create the Notification table
CREATE TABLE notification (
  id SERIAL PRIMARY KEY,
  user_id INT NOT NULL,
  message TEXT NOT NULL,
  sent_time TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (user_id) REFERENCES User(id)
);

-- Insert initial data into the Machine table
INSERT INTO Machine (name, location) VALUES
  ('Machine A', 'Room 101'),
  ('Machine B', 'Room 102');

-- Insert initial data into the User table
INSERT INTO User (name, email) VALUES
  ('John Doe', 'john.doe@example.com'),
  ('Jane Doe', 'jane.doe@example.com');

-- Insert initial data into the MachineReservation table
INSERT INTO MachineReservation (machine_id, start_time, end_time, status) VALUES
  (1, '2024-03-10 10:00:00', '2024-03-10 12:00:00', 'Pending');

-- Insert initial data into the Schedule table
INSERT INTO Schedule (user_id, reservation_id, start_time, end_time, status) VALUES
  (1, 1, '2024-03-10 10:00:00', '2024-03-10 12:00:00', 'Confirmed');

-- Insert initial data into the Notification table
INSERT INTO Notification (user_id, message) VALUES
  (1, 'You have a new reservation on Machine A!');