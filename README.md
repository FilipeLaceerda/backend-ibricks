# Backend Ibricks

## Pré-requisitos

## Instale o Docker em sua máquina:

**Passo 1: Verifique se o Docker já está instalado**

Abra seu terminal e execute o seguinte comando:

```bash
docker --version
```

Se o Docker estiver instalado, você verá a versão do Docker. Caso contrário, siga os passos abaixo para a instalação.

**Passo 2: Baixe o Docker**

**Para Windows:**

* Acesse o site oficial do Docker ([https://www.docker.com/products/docker-desktop/](https://www.docker.com/products/docker-desktop/)) e baixe o instalador para Windows.
* Execute o instalador e siga as instruções na tela.

**Para macOS:**

* Acesse o site oficial do Docker ([https://www.docker.com/products/docker-desktop/](https://www.docker.com/products/docker-desktop/)) e baixe o instalador para macOS.
* Execute o instalador e siga as instruções na tela.

**Para Linux:**

* Abra seu terminal e execute os seguintes comandos:

  ```bash
  sudo apt-get update
  sudo apt-get install docker.io
  ```

* Se você estiver usando uma distribuição Linux diferente do Ubuntu, consulte a documentação oficial do Docker para instruções específicas da sua distribuição.

### Execução do projeto com Docker Compose

1. **Navegue até à raiz do seu projeto** no seu terminal ou prompt de comando.
2. **Execute o comando `docker-compose up`**. Isto irá construir e iniciar todos os serviços definidos no seu ficheiro `docker-compose.yml`.

```bash
docker-compose up -d
```
## Instalação do Siege

O Siege é uma ferramenta de teste de carga de código aberto que simula vários usuários acessando um servidor Web para testar seu desempenho e estabilidade. Aqui estão as instruções para instalar o Siege em diferentes sistemas operacionais:

### Linux

**1. Instale a partir do repositório:**

```bash
sudo apt update
sudo apt install siege
```

**2. Instale a partir do código fonte:**

* Baixe o código fonte do Siege [https://github.com/JoeDog/siege/](https://github.com/JoeDog/siege/).
* Extraia o arquivo.
* Navegue para o diretório extraído.
* Execute o script de configuração: `./configure`
* Compile o Siege: `make`
* Instale o Siege: `sudo make install`

### macOS

**1. Instale usando Homebrew:**

```bash
brew install siege
```

**2. Instale a partir do código fonte:**

* Siga as instruções para Linux, começando do passo 2.

### Windows

**1. Instale o Siege usando o pacote binário:**

* Baixe o pacote binário do Siege para Windows [https://github.com/JoeDog/siege/releases](https://github.com/JoeDog/siege/releases).
* Extraia o arquivo.
* Adicione o diretório de instalação do Siege à sua variável de ambiente `PATH`.

**2. Instale o Siege usando Cygwin:**

* Instale o Cygwin [https://www.cygwin.com/](https://www.cygwin.com/).
* No instalador do Cygwin, selecione o pacote "Siege".
* Após a instalação, o Siege estará disponível no Cygwin.

### Usando o Siege

Após a instalação, você pode executar o Siege com a seguinte sintaxe:

```bash
siege [opções] [URL]
```

**Exemplos:**

* Executar um teste de 10 usuários simulando 10 segundos de atividade: `siege -c 10 -t 10s http://localhost:4003/stores`
* Executar um teste de 20 usuários simulando um ataque de 1 minuto: `siege -c 20 -t 1m http://localhost:4003/stores`
* Executar um teste de 50 usuários com 5 segundos de espera entre as requisições: `siege -c 50 -r 5 http://localhost:4003/stores`

# Acessando o Grafana e o Zipkin UI

## Acessando o Grafana

1. **Abra seu navegador web.**
2. **Digite o seguinte endereço na barra de endereço:** `http://localhost:3000`
3. No canto esquerdo clique em dashboard.
4. No canto superior direito clique em new e depois em new dashboard
5. Clique em Add Visualization.
6. Selecione Loki.
7. Embaixo em labels filters selecione: select label = job e select value = store e clique no botão azul run query
8. Para melhor visualização dos logs no canto superior esquerdo trocar de time series para logs.


![alt text](assets/Logs.png) 


9. **Passos para o Prometheus:**
10. Siga do passo 1 até o 5.
11. Selecione Prometheus.
12. Em metric selecione store_service_total.
13. Clique no botão run queries e no canto superior esquerdo selecione ou time series ou histogram.
14. Para melhor visualização de um gráfico de status por tempo levado para a requisição a opção histogram é mais aconselhada.


![alt text](assets/Prometheus.png)

15. Outra maneira de ver as metricas é pela a UI do prometheus, acessando localhost:9090
16. Clicando no botão que tem um ícone de mundo, procurar por "store_service_total", selecionar e clicar em execute
17. Para melhor visualização clicar em graph.

![alt text](assets/metricas-prometheus.png)


## Acessando o Zipkin UI

1. **Abra seu navegador web.**
2. **Digite o seguinte endereço na barra de endereço:** `http://localhost:9411`
3. Clique em RUN QUERY, ele vem com uma configuração padrão de limit de 10 traces nos últimos 15 minutos.
4. Clicando no botão da engrenagem tem como mudar essa configuração
5. Clicando no botão + vermelho no canto superior esquerdo tem algumas opções de queries, selecionando serviceName tem como selecionar store e rodar só esse serviço

![alt text](assets/zipkin.png)

6. Clicando no botão SHOW, mostra os detalhes desse rastreio.

![alt text](assets/detalhe_zipkin.png)

## Informações adicionais

* Se você não conseguir acessar o Grafana ou o Zipkin UI, verifique se os serviços estão em execução e se as portas corretas estão abertas.
